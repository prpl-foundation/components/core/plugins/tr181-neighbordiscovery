# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.1.10 - 2024-12-11(15:14:03 +0000)

### Other

- RS sent by the DUT after Reboot or upon WAN intf bounce are without Source link-layer address option

## Release v1.1.9 - 2024-10-24(10:28:53 +0000)

### Other

- No ipv6 : RA is received too early

## Release v1.1.8 - 2024-09-10(07:14:50 +0000)

### Other

- [AppArmor] Create AppAmor profile for plugins

## Release v1.1.7 - 2024-07-29(06:07:46 +0000)

### Other

- - [amx] Failing to restart processes with init scripts

## Release v1.1.6 - 2024-07-10(06:39:21 +0000)

### Other

- No static IPv6 after external disable_ipv6 toggle

## Release v1.1.5 - 2024-06-25(13:18:42 +0000)

### Other

- - no-wan-dhcpv6-pd-prefix

## Release v1.1.4 - 2024-06-04(17:57:50 +0000)

### Fixes

- add support for creating bridge interfaces

## Release v1.1.3 - 2024-06-04(14:24:06 +0000)

### Other

- add support for creating bridge interfaces

## Release v1.1.2 - 2024-05-30(10:33:04 +0000)

### Other

- add support for creating new bridge interfaces

## Release v1.1.1 - 2024-04-10(07:11:22 +0000)

### Changes

- Make amxb timeouts configurable

## Release v1.1.0 - 2024-03-23(13:09:18 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v1.0.0 - 2024-03-22(08:15:58 +0000)

### Breaking

- GUA_RA address toggles when receiving for the first time

## Release v0.8.4 - 2024-03-16(17:13:02 +0000)

### Other

- Rework TR-181 interface stacks indexing number

## Release v0.8.3 - 2023-12-12(11:34:16 +0000)

### Fixes

- [Repeater Config][IPv6] Implement a proper IPv6 config of the repeater

## Release v0.8.2 - 2023-11-30(10:58:09 +0000)

### Fixes

- WAN port instability

## Release v0.8.1 - 2023-10-13(13:47:27 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v0.8.0 - 2023-10-09(07:49:55 +0000)

### New

- [amxrt][no-root-user][capability drop]tr181-neighbordiscovery must be adapted to run as non-root and lmited capabilities

## Release v0.7.3 - 2023-09-21(15:43:18 +0000)

### Fixes

- PPP6 LLA address is often automatically erronously removed

## Release v0.7.2 - 2023-07-06(10:02:33 +0000)

### Fixes

- [prplOS-next][IPv6]Box does Not send Router Solicitation on WAN upon interface is Up

## Release v0.7.1 - 2023-07-03(11:49:15 +0000)

### Fixes

- [prplOS-next][IPv6]Box does Not send Router Solicitation on WAN upon interface is Up

## Release v0.7.0 - 2023-05-09(14:05:36 +0000)

### New

- [ipv6] Support for accept_ra_defrtr procfs entry

## Release v0.6.8 - 2023-05-05(13:39:49 +0000)

### Fixes

- [TR-181][IP-Manager] For PPP the IPv6 address should not be set automatically

## Release v0.6.7 - 2023-05-04(10:03:25 +0000)

### Fixes

- [TR181-Neighbordiscovery] Persistency does not work

## Release v0.6.6 - 2023-03-21(14:21:21 +0000)

### Fixes

- [NetModel] NetDev information in netmodel ppp-wan interface is not cleared when ppp connection stops

## Release v0.6.5 - 2023-03-17(18:46:41 +0000)

### Other

- [baf] Correct typo in config option

## Release v0.6.4 - 2023-03-16(14:25:07 +0000)

### Other

- Add AP config files

## Release v0.6.3 - 2023-02-16(10:23:06 +0000)

### Other

- Add missing STAGING_LIBDIR to LDFLAGS

## Release v0.6.2 - 2023-02-14(12:18:08 +0000)

### Fixes

- [tr181-ppp] Sometimes there is no default route for ppp6

## Release v0.6.1 - 2023-01-09(09:38:36 +0000)

### Fixes

- avoidable allocation with amxc_string_dup

## Release v0.6.0 - 2022-12-15(12:09:25 +0000)

### New

- Add ipv4/6 netmodel flag to netdevname query

## Release v0.5.0 - 2022-11-03(08:34:22 +0000)

### New

- switched to the logical interface for cpe-wan

## Release v0.4.1 - 2022-11-03(08:14:14 +0000)

### Fixes

- after boot, GUA but no ipv6 default route, until new RA received

## Release v0.4.0 - 2022-10-14(12:06:23 +0000)

### New

- Update the README

## Release v0.3.5 - 2022-10-14(07:35:10 +0000)

### Fixes

- [NeighbourDiscovery][Firewall]Too many ports are opened by the neighbourdiscovery plugin

## Release v0.3.4 - 2022-08-19(08:39:05 +0000)

### Fixes

- Add fw rule faster

## Release v0.3.3 - 2022-07-18(14:40:15 +0000)

### Fixes

- [tr181-neighbordiscovery] allow icmpv6 packet on the firewall

## Release v0.3.2 - 2022-07-04(15:34:00 +0000)

### Other

- Opensource component

## Release v0.3.1 - 2022-06-28(07:44:17 +0000)

### Other

- [neighbourdiscovery]change netmodel query from netdev-up to netdev-bound

## Release v0.3.0 - 2022-04-01(12:36:36 +0000)

### New

- [TR181][Neighbordiscovery] copy variables to proc files

## Release v0.2.1 - 2022-03-24(10:45:52 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v0.2.0 - 2022-03-12(07:20:21 +0000)

### New

- [TR181][Neighbordiscovery] create definition odl

## Release v0.1.0 - 2022-03-11(09:37:20 +0000)

### New

- [TR181][Neighbordiscovery]make the base of the Neighbordiscovery plugin

