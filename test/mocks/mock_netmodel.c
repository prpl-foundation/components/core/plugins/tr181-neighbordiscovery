/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "mock.h"

#include <setjmp.h>
#include <cmocka.h>

#include <stdlib.h>
#include <string.h>

static amxp_slot_fn_t netdev_bound_cb;
static amxp_slot_fn_t netdev_up_cb;
static amxp_slot_fn_t mac_cb;
static void* netdev_bound_userdata;
static void* netdev_up_userdata;
static void* mac_userdata;

static const char* nm_flags = "";

bool __wrap_netmodel_initialize(void) {
    return true;
}

void __wrap_netmodel_cleanup(void) {
    return;
}

void set_nm_flags(const char* flags) {
    nm_flags = flags;
}

bool __wrap_netmodel_hasFlag(UNUSED const char* intf,
                             const char* flag,
                             UNUSED const char* condition,
                             UNUSED const char* traverse) {
    bool rv = false;
    if(strstr(nm_flags, flag) != NULL) {
        rv = true;
    }
    return rv;
}

netmodel_query_t* __wrap_netmodel_openQuery_getFirstParameter(const char* intf,
                                                              const char* subscriber,
                                                              const char* name,
                                                              UNUSED const char* flag,
                                                              const char* traverse,
                                                              amxp_slot_fn_t handler,
                                                              void* userdata) {
    netmodel_query_t* q = NULL;

    assert_non_null(traverse);
    assert_non_null(handler);
    assert_non_null(name);
    assert_non_null(subscriber);

    if(strncmp(intf, "Device.IP.Interface.", 20) != 0) {
        goto exit;
    }

    if(strcmp(name, "MACAddress") == 0) {
        mac_cb = handler;
        mac_userdata = userdata;
    } else if(strcmp(flag, "netdev-up && ipv6") == 0) {
        netdev_up_cb = handler;
        netdev_up_userdata = userdata;
    } else {
        netdev_bound_cb = handler;
        netdev_bound_userdata = userdata;
    }

    q = malloc(sizeof(netmodel_query_t*));
exit:
    return q;
}

void trigger_netdev_callback(amxc_var_t* data, bool netdev_up) {
    if(netdev_up) {
        assert_non_null(netdev_up_cb);
        assert_non_null(netdev_up_userdata);
        netdev_up_cb(NULL, data, netdev_up_userdata);
    } else {
        assert_non_null(netdev_bound_cb);
        assert_non_null(netdev_bound_userdata);
        netdev_bound_cb(NULL, data, netdev_bound_userdata);
    }
}

void trigger_mac_callback(amxc_var_t* data) {
    assert_non_null(mac_cb);
    assert_non_null(mac_userdata);
    mac_cb(NULL, data, mac_userdata);
}

void __wrap_netmodel_closeQuery(netmodel_query_t* query) {
    free(query);
}


