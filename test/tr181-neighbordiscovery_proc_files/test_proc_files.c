/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_proc_files.h"
#include "neighbordiscovery.h"
#include "test_utils.h"

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include "../mocks/mock.h"
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#define RS_HEADER_SIZE 8
#define RS_SLLA_SIZE 8

static void expect_how_the_files_are_written(bool enabled) {
    expect_string_count(__wrap_fopen, pathname, "/proc/sys/net/ipv6/conf/eth0/dad_transmits", 2);
    expect_string_count(__wrap_fopen, pathname, "/proc/sys/net/ipv6/neigh/eth0/retrans_time_ms", 2);
    expect_string_count(__wrap_fopen, pathname, "/proc/sys/net/ipv6/conf/eth0/router_solicitation_max_interval", 2);
    expect_string_count(__wrap_fopen, pathname, "/proc/sys/net/ipv6/conf/eth0/router_solicitation_interval", 2);
    expect_string_count(__wrap_fopen, pathname, "/proc/sys/net/ipv6/conf/eth0/router_solicitations", 2);

    if(enabled) {
        expect_string_count(__wrap_fopen, pathname, "/proc/sys/net/ipv6/conf/eth0/accept_ra", 2);
        expect_string(__wrap_fopen, pathname, "/proc/sys/net/ipv6/conf/eth0/forwarding");
        expect_string(__wrap_fopen, pathname, "/proc/sys/net/ipv6/conf/eth0/autoconf");
    } else {
        expect_string(__wrap_fopen, pathname, "/proc/sys/net/ipv6/conf/eth0/accept_ra");
        expect_string_count(__wrap_fopen, pathname, "/proc/sys/net/ipv6/conf/eth0/forwarding", 2);
        expect_string_count(__wrap_fopen, pathname, "/proc/sys/net/ipv6/conf/eth0/autoconf", 2);
    }
    expect_string_count(__wrap_fopen, pathname, "/proc/sys/net/ipv6/conf/eth0/accept_ra_defrtr", 2);
}

static void add_interface(const cstring_t alias, const cstring_t intf_name, uint32_t dad_transmits, uint32_t retrans_timer, uint32_t rtr_sol_intv, uint32_t max_rtr_sol, bool nud_enable, bool rs_enable) {
    amxd_trans_t trans;
    amxd_object_t* neighbor_interface_obj = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    amxd_trans_init(&trans);

    neighbor_interface_obj = amxd_dm_findf(test_get_dm(), "NeighborDiscovery.InterfaceSetting.");
    assert_non_null(neighbor_interface_obj);

    amxd_trans_select_object(&trans, neighbor_interface_obj);
    amxd_trans_add_inst(&trans, 0, NULL);

    amxd_trans_set_value(cstring_t, &trans, "Alias", alias);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", intf_name);
    amxd_trans_set_value(uint32_t, &trans, "DADTransmits", dad_transmits);
    amxd_trans_set_value(uint32_t, &trans, "RetransTimer", retrans_timer);
    amxd_trans_set_value(uint32_t, &trans, "RtrSolicitationInterval", rtr_sol_intv);
    amxd_trans_set_value(uint32_t, &trans, "MaxRtrSolicitations", max_rtr_sol);
    amxd_trans_set_value(bool, &trans, "NUDEnable", nud_enable);
    amxd_trans_set_value(bool, &trans, "RSEnable", rs_enable);

    status = amxd_trans_apply(&trans, test_get_dm());
    assert_int_equal(status, amxd_status_ok);

    amxd_trans_clean(&trans);
    test_handle_events();
}

static void del_interface(const cstring_t alias) {
    amxd_trans_t trans;
    amxd_object_t* neighbor_interface_obj = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    amxd_trans_init(&trans);

    neighbor_interface_obj = amxd_dm_findf(test_get_dm(), "NeighborDiscovery.InterfaceSetting.");
    assert_non_null(neighbor_interface_obj);

    amxd_trans_select_object(&trans, neighbor_interface_obj);
    amxd_trans_del_inst(&trans, 0, alias);

    status = amxd_trans_apply(&trans, test_get_dm());
    assert_int_equal(status, amxd_status_ok);

    amxd_trans_clean(&trans);
    test_handle_events();
}

static void del_all_interfaces(void) {
    cstring_t alias = NULL;

    amxd_object_t* neighbordiscovery_obj = amxd_dm_findf(neighbordiscovery_get_dm(), "NeighborDiscovery.InterfaceSetting.");
    amxd_object_for_each(instance, it, neighbordiscovery_obj) {
        amxd_object_t* inst = amxc_container_of(it, amxd_object_t, it);

        alias = amxd_object_get_value(cstring_t, inst, "Alias", NULL);
        del_interface(alias);
        test_handle_events();
        free(alias);
    }
}

static void enable_intf(cstring_t alias, bool enable) {
    amxd_trans_t trans;
    amxd_object_t* neighbor_interface_obj = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    amxd_trans_init(&trans);

    neighbor_interface_obj = amxd_dm_findf(test_get_dm(), "NeighborDiscovery.InterfaceSetting.[Alias == '%s']", alias);
    assert_non_null(neighbor_interface_obj);

    amxd_trans_select_object(&trans, neighbor_interface_obj);

    amxd_trans_set_value(bool, &trans, "Enable", enable);

    status = amxd_trans_apply(&trans, test_get_dm());
    assert_int_equal(status, amxd_status_ok);

    amxd_trans_clean(&trans);
}

static void set_dad_transmits_value(const cstring_t alias, uint32_t new_value) {
    amxd_trans_t trans;
    amxd_object_t* neighbor_interface_obj = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    amxd_trans_init(&trans);

    neighbor_interface_obj = amxd_dm_findf(test_get_dm(), "NeighborDiscovery.InterfaceSetting.[Alias == '%s']", alias);
    assert_non_null(neighbor_interface_obj);

    amxd_trans_select_object(&trans, neighbor_interface_obj);

    amxd_trans_set_value(uint32_t, &trans, "DADTransmits", new_value);

    status = amxd_trans_apply(&trans, test_get_dm());
    assert_int_equal(status, amxd_status_ok);

    amxd_trans_clean(&trans);
}

static void enable_parent(bool enable) {
    amxd_trans_t trans;
    amxd_object_t* neighbor_interface_obj = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    amxd_trans_init(&trans);

    neighbor_interface_obj = amxd_dm_findf(test_get_dm(), "NeighborDiscovery.");
    assert_non_null(neighbor_interface_obj);

    amxd_trans_select_object(&trans, neighbor_interface_obj);

    amxd_trans_set_value(bool, &trans, "Enable", enable);

    status = amxd_trans_apply(&trans, test_get_dm());
    assert_int_equal(status, amxd_status_ok);

    amxd_trans_clean(&trans);
}

void test_add_interface_setting(UNUSED void** state) {
    amxd_object_t* neighbor_interface_obj = NULL;
    amxc_var_t data_cb;
    cstring_t intf_name = "eth0";
    const char* status = NULL;

    amxc_var_init(&data_cb);

    //setup the interface
    enable_parent(true);
    add_interface("test_add", "Device.IP.Interface.2.", 5, 4000, 4000, 4, true, true);
    neighbor_interface_obj = amxd_dm_findf(test_get_dm(), "NeighborDiscovery.InterfaceSetting.[Alias == 'test_add']");
    assert_non_null(neighbor_interface_obj);

    status = GET_CHAR(amxd_object_get_param_value(neighbor_interface_obj, "Status"), NULL);
    assert_string_equal(status, "Disabled");

    //turn on the mocked versions of: fopen, fclose, ...
    set_call_mocked_file_functions(true);
    //Make ready, to Check if all the files are being written to
    expect_how_the_files_are_written(true);
    //trigger netmodel event, which will call the fopen events and trigger the 'expect_how_the_files_are_written' calls
    amxc_var_set(cstring_t, &data_cb, intf_name);
    trigger_netdev_callback(&data_cb, false);
    //turn off the mocked versions
    set_call_mocked_file_functions(false);

    //check if everything is called correctly
    status = GET_CHAR(amxd_object_get_param_value(neighbor_interface_obj, "Status"), NULL);
    assert_string_equal(status, "Enabled");

    amxc_var_clean(&data_cb);
}



void test_rm_interface_setting(UNUSED void** state) {
    amxd_object_t* neighbor_interface_obj = NULL;
    amxc_var_t data_cb;
    cstring_t intf_name = "eth0";
    const char* status = NULL;

    amxc_var_init(&data_cb);

    //delete other interfaces -> otherwise more __wrap_fopen are called (for each interface) -> harder to catch the one we expect
    del_all_interfaces();

    add_interface("test_rm", "Device.IP.Interface.2.", 5, 4000, 4000, 4, true, true);
    neighbor_interface_obj = amxd_dm_findf(test_get_dm(), "NeighborDiscovery.InterfaceSetting.[Alias == 'test_rm']");
    assert_non_null(neighbor_interface_obj);

    //check if the setting of files is correctly
    set_call_mocked_file_functions(true);
    expect_how_the_files_are_written(true);
    //trigger netmodel event
    amxc_var_set(cstring_t, &data_cb, intf_name);
    trigger_netdev_callback(&data_cb, false);
    set_call_mocked_file_functions(false);

    //check if everything is called correctly
    status = GET_CHAR(amxd_object_get_param_value(neighbor_interface_obj, "Status"), NULL);
    assert_string_equal(status, "Enabled");

    //Now remove the instance
    set_call_mocked_file_functions(true);
    expect_how_the_files_are_written(false);
    del_interface("test_rm");
    test_handle_events();
    set_call_mocked_file_functions(false);

    neighbor_interface_obj = amxd_dm_findf(test_get_dm(), "NeighborDiscovery.InterfaceSetting.[Alias == 'test_rm']");
    assert_null(neighbor_interface_obj);

    amxc_var_clean(&data_cb);
}

void test_netmodel_intf_goes_down(UNUSED void** state) {
    amxd_object_t* neighbor_interface_obj = NULL;
    amxc_var_t data_cb;
    cstring_t intf_name = "eth0";
    const char* status = NULL;

    amxc_var_init(&data_cb);

    //setup the interface
    enable_parent(true);
    add_interface("test_intf_down", "Device.IP.Interface.2.", 5, 4000, 4000, 4, true, true);
    neighbor_interface_obj = amxd_dm_findf(test_get_dm(), "NeighborDiscovery.InterfaceSetting.[Alias == 'test_intf_down']");

    //check if the setting of files is correctly
    set_call_mocked_file_functions(true);
    expect_how_the_files_are_written(true);
    //trigger netmodel event
    amxc_var_set(cstring_t, &data_cb, intf_name);
    trigger_netdev_callback(&data_cb, false);
    set_call_mocked_file_functions(false);

    //check if everything is called correctly
    status = GET_CHAR(amxd_object_get_param_value(neighbor_interface_obj, "Status"), NULL);
    assert_string_equal(status, "Enabled");

    //-> set interface offline
    expect_how_the_files_are_written(false);
    set_call_mocked_file_functions(true);
    amxc_var_set(cstring_t, &data_cb, "");
    trigger_netdev_callback(&data_cb, false);
    set_call_mocked_file_functions(false);

    status = GET_CHAR(amxd_object_get_param_value(neighbor_interface_obj, "Status"), NULL);
    assert_string_equal(status, "Disabled");

    amxc_var_clean(&data_cb);
}

void test_global_enable(UNUSED void** state) {
    amxd_object_t* neighbor_interface_obj = NULL;
    amxc_var_t data_cb;
    cstring_t intf_name = "eth0";
    const char* status = NULL;

    amxc_var_init(&data_cb);

    //delete other interfaces -> otherwise more __wrap_fopen are called (for each interface) -> harder to catch the one we expect
    del_all_interfaces();

    //setup the interface
    enable_parent(true);
    add_interface("test_disable_parent", "Device.IP.Interface.2.", 5, 4000, 4000, 4, true, true);
    neighbor_interface_obj = amxd_dm_findf(test_get_dm(), "NeighborDiscovery.InterfaceSetting.[Alias == 'test_disable_parent']");

    //check if the setting of files is correctly
    set_call_mocked_file_functions(true);
    expect_how_the_files_are_written(true);
    //trigger netmodel event
    amxc_var_set(cstring_t, &data_cb, intf_name);
    trigger_netdev_callback(&data_cb, false);
    set_call_mocked_file_functions(false);

    //check if everything is called correctly
    status = GET_CHAR(amxd_object_get_param_value(neighbor_interface_obj, "Status"), NULL);
    assert_string_equal(status, "Enabled");

    enable_parent(false);
    expect_how_the_files_are_written(false);
    set_call_mocked_file_functions(true);
    test_handle_events();
    set_call_mocked_file_functions(false);

    status = GET_CHAR(amxd_object_get_param_value(neighbor_interface_obj, "Status"), NULL);
    assert_string_equal(status, "Disabled");

    amxc_var_clean(&data_cb);
}

void test_enable(UNUSED void** state) {
    amxd_object_t* neighbor_interface_obj = NULL;
    amxc_var_t data_cb;
    cstring_t intf_name = "eth0";
    const char* status = NULL;

    amxc_var_init(&data_cb);

    //setup the interface
    enable_parent(true);
    add_interface("test_disable", "Device.IP.Interface.2.", 5, 4000, 4000, 4, true, true);
    neighbor_interface_obj = amxd_dm_findf(test_get_dm(), "NeighborDiscovery.InterfaceSetting.[Alias == 'test_disable']");

    //check if the setting of files is correctly
    set_call_mocked_file_functions(true);
    expect_how_the_files_are_written(true);
    //trigger netmodel event
    amxc_var_set(cstring_t, &data_cb, intf_name);
    trigger_netdev_callback(&data_cb, false);
    set_call_mocked_file_functions(false);

    //check if everything is called correctly
    status = GET_CHAR(amxd_object_get_param_value(neighbor_interface_obj, "Status"), NULL);
    assert_string_equal(status, "Enabled");

    //disable the intf
    enable_intf("test_disable", false);
    expect_how_the_files_are_written(false);
    set_call_mocked_file_functions(true);
    test_handle_events();
    set_call_mocked_file_functions(false);

    status = GET_CHAR(amxd_object_get_param_value(neighbor_interface_obj, "Status"), NULL);
    assert_string_equal(status, "Disabled");

    //enable the intf again
    enable_intf("test_disable", true);
    expect_how_the_files_are_written(true);
    set_call_mocked_file_functions(true);
    test_handle_events();
    trigger_netdev_callback(&data_cb, false);
    set_call_mocked_file_functions(false);

    //check if everything is called correctly
    status = GET_CHAR(amxd_object_get_param_value(neighbor_interface_obj, "Status"), NULL);
    assert_string_equal(status, "Enabled");

    amxc_var_clean(&data_cb);
}

void test_param_changed(UNUSED void** state) {
    amxd_object_t* neighbor_interface_obj = NULL;
    amxc_var_t data_cb;
    cstring_t intf_name = "eth0";
    const char* status = NULL;

    amxc_var_init(&data_cb);

    //setup the interface
    enable_parent(true);
    add_interface("test_change_intf", "Device.IP.Interface.2.", 5, 4000, 4000, 4, true, true);
    neighbor_interface_obj = amxd_dm_findf(test_get_dm(), "NeighborDiscovery.InterfaceSetting.[Alias == 'test_change_intf']");
    assert_non_null(neighbor_interface_obj);

    status = GET_CHAR(amxd_object_get_param_value(neighbor_interface_obj, "Status"), NULL);
    assert_string_equal(status, "Disabled");

    //check if the setting of files is correctly
    set_call_mocked_file_functions(true);
    expect_how_the_files_are_written(true);
    //trigger netmodel event
    amxc_var_set(cstring_t, &data_cb, intf_name);
    trigger_netdev_callback(&data_cb, false);
    set_call_mocked_file_functions(false);

    //check if everything is called correctly
    status = GET_CHAR(amxd_object_get_param_value(neighbor_interface_obj, "Status"), NULL);
    assert_string_equal(status, "Enabled");

    set_dad_transmits_value("test_change_intf", 8);
    //now only the dad_transmits param should be changed
    expect_string_count(__wrap_fopen, pathname, "/proc/sys/net/ipv6/conf/eth0/dad_transmits", 2);
    set_call_mocked_file_functions(true);
    test_handle_events();
    set_call_mocked_file_functions(false);

    amxc_var_clean(&data_cb);
}

void test_bad_netmodel_intf_value(UNUSED void** state) {
    amxd_object_t* neighbor_interface_obj = NULL;
    amxc_var_t params;

    amxc_var_init(&params);

    //setup the interface
    enable_parent(true);
    add_interface("test_bad_intf_value", "Bad value", 5, 4000, 4000, 4, true, true);
    neighbor_interface_obj = amxd_dm_findf(test_get_dm(), "NeighborDiscovery.InterfaceSetting.[Alias == 'test_bad_intf_value']");
    assert_non_null(neighbor_interface_obj);

    assert_int_equal(amxd_object_get_params(neighbor_interface_obj, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Error_Misconfigured");

    amxc_var_clean(&params);
}

void test_send_rs(UNUSED void** state) {
    amxd_object_t* neighbor_interface_obj = NULL;
    amxc_var_t data_cb;
    amxc_var_t mac;
    cstring_t intf_name = "eth0";
    const char* status = NULL;

    amxc_var_init(&data_cb);
    amxc_var_init(&mac);

    //setup the interface
    enable_parent(true);
    add_interface("test_send_rs", "Device.IP.Interface.2.", 5, 4000, 4000, 4, true, true);
    neighbor_interface_obj = amxd_dm_findf(test_get_dm(), "NeighborDiscovery.InterfaceSetting.[Alias == 'test_send_rs']");
    assert_non_null(neighbor_interface_obj);

    status = GET_CHAR(amxd_object_get_param_value(neighbor_interface_obj, "Status"), NULL);
    assert_string_equal(status, "Disabled");

    set_call_mocked_file_functions(true);
    expect_how_the_files_are_written(true);
    //trigger netmodel events, which will call the RS timer cb
    amxc_var_set(cstring_t, &data_cb, intf_name);
    trigger_netdev_callback(&data_cb, false);
    trigger_netdev_callback(&data_cb, true);

    // trigger mac callback to set empty MAC address
    amxc_var_set(cstring_t, &mac, "");
    trigger_mac_callback(&mac);

    set_call_mocked_file_functions(false);

    // check that RS is sent without Source Link Layer Address
    set_bytes_sent(RS_HEADER_SIZE);
    expect_function_call(__wrap_socket);
    expect_function_calls(__wrap_setsockopt, 4);
    expect_function_call(__wrap_sendto);
    expect_function_call(__wrap_close);

    // let the RS timer expire
    read_sig_alarm();

    // check that RS is sent with Source Link Layer Address
    amxc_var_set(cstring_t, &mac, "C0:3C:04:BE:36:50");
    trigger_mac_callback(&mac);
    set_bytes_sent(RS_HEADER_SIZE + RS_SLLA_SIZE);
    expect_function_call(__wrap_socket);
    expect_function_calls(__wrap_setsockopt, 4);
    expect_function_call(__wrap_sendto);
    expect_function_call(__wrap_close);

    // let the RS timer expire
    read_sig_alarm();

    // indicate that we have received an RA so we should not send any more RS
    set_nm_flags("iprouter-up");
    read_sig_alarm();

    // check if everything is called correctly
    status = GET_CHAR(amxd_object_get_param_value(neighbor_interface_obj, "Status"), NULL);
    assert_string_equal(status, "Enabled");

    amxc_var_clean(&data_cb);
    amxc_var_clean(&mac);
}