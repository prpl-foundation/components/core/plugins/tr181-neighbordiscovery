{% let hasAnyUpstream = BDfn.hasAnyUpstream() %}
// default value(s) for Device.IP.Interface reference
%config {
    %global ip_intf_lan = "Device.IP.Interface.3.";
    %global ip_intf_guest = "Device.IP.Interface.4.";
}
#include "global_ip_interfaces.odl";

%populate {
    object NeighborDiscovery {
        parameter Enable = true;

        object InterfaceSetting {
{% if (hasAnyUpstream) : %}
            instance add ("cpe-wan"){
                parameter Enable = true;
                parameter Alias = "cpe-wan";
                parameter Interface = "Device.Logical.Interface.1.";
                parameter RSEnable = true;
            }
{% endif; %}
{% for (let Bridge in BD.Bridges) : %}
            instance add ("cpe-{{lc(Bridge)}}"){
                parameter Enable = true;
                parameter Alias = "cpe-{{lc(Bridge)}}";
                parameter Interface = "${ip_intf_{{lc(Bridge)}}}";
                parameter RSEnable ={% if (hasAnyUpstream) : %} false{% else %} true{% endif %};
                parameter AcceptDefaultRoute ={% if (hasAnyUpstream) : %} false{% else %} true{% endif %};
            }
{% endfor %}
        }
    }
}
