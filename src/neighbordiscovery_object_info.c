/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**********************************************************
* Include files
**********************************************************/

#include "neighbordiscovery.h"
#include "neighbordiscovery_object_info.h"
#include "neighbordiscovery_netmodel_intf.h"
#include "neighbordiscovery_protocol.h"
#include "firewall_utility.h"

#include <stdlib.h>

/**********************************************************
* Macro definitions
**********************************************************/

/**********************************************************
* Type definitions
**********************************************************/

/**********************************************************
* Variable declarations
**********************************************************/

/**********************************************************
* Function Prototypes
**********************************************************/

/**********************************************************
* Functions
**********************************************************/

amxd_status_t neighbordiscovery_info_create(amxd_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    neigh_info_t* neigh_info = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    amxc_string_t alias_str;

    amxc_string_init(&alias_str, 0);

    amxc_string_setf(&alias_str, FW_ALIAS_PREFIX "%d", obj->index);

    when_null_trace(obj, exit, ERROR, "invalid obj given");
    when_false_trace(obj->priv == NULL, exit, ERROR, "Stop creating a neighbordiscovery info, private data is not NULL");

    neigh_info = (neigh_info_t*) calloc(1, sizeof(neigh_info_t));
    when_null_trace(neigh_info, exit, ERROR, "Failed to calloc a neigh_info_t");

    obj->priv = neigh_info;
    neigh_info->obj = obj;
    neigh_info->intf_path = NULL;
    neigh_info->intf_name = NULL;
    neigh_info->intf_query = NULL;
    neigh_info->alias_fw_rule = amxc_string_take_buffer(&alias_str);

    when_failed_trace(amxp_timer_new(&(neigh_info->rs_info.rs_timer), rs_timer_cb, neigh_info), exit, ERROR, "Failed to initialize RS timer");

    status = amxd_status_ok;
exit:
    amxc_string_clean(&alias_str);
    SAH_TRACEZ_OUT(ME);
    return status;
}

static void rs_info_clear(rs_info_t* rs_info) {
    when_null(rs_info, exit);

    if(rs_info->rs_timer != NULL) {
        amxp_timer_delete(&(rs_info->rs_timer));
        rs_info->rs_timer = NULL;
    }
    if(rs_info->mac_query != NULL) {
        netmodel_closeQuery(rs_info->mac_query);
        rs_info->mac_query = NULL;
    }
    free(rs_info->mac);
    rs_info->mac = NULL;

exit:
    return;
}

void neighbordiscovery_info_clear(amxd_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    neigh_info_t* neigh_info = NULL;

    when_null_trace(obj, exit, ERROR, "no valid object given");
    neigh_info = (neigh_info_t*) obj->priv;
    when_null_trace(neigh_info, exit, ERROR, "object does not have valid neigh_info");

    neighbordiscovery_info_clear_content(neigh_info);

    // The name should not change during the full lifetime of info structure, so only clear it if we clear the info struct itself
    free(neigh_info->alias_fw_rule);
    neigh_info->alias_fw_rule = NULL;

    rs_info_clear(&neigh_info->rs_info);

    neigh_info->obj = NULL;
    free(neigh_info);
    obj->priv = NULL;
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void neighbordiscovery_info_clear_content(neigh_info_t* neigh_info) {
    SAH_TRACEZ_IN(ME);
    when_null_trace(neigh_info, exit, ERROR, "object does not have valid neigh_info");

    if(neigh_info->intf_query != NULL) {
        netmodel_closeQuery(neigh_info->intf_query);
        neigh_info->intf_query = NULL;
    }

    if(neigh_info->netdev_up_query != NULL) {
        netmodel_closeQuery(neigh_info->netdev_up_query);
        neigh_info->netdev_up_query = NULL;
    }

    free(neigh_info->intf_name);
    neigh_info->intf_name = NULL;

    free(neigh_info->intf_path);
    neigh_info->intf_path = NULL;

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}
