/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**********************************************************
* Include files
**********************************************************/

#include "neighbordiscovery.h"
#include "neighbordiscovery_netmodel_intf.h"
#include "netmodel/client.h"

#include <stdio.h>

/**********************************************************
* Macro definitions
**********************************************************/

/**********************************************************
* Type definitions
**********************************************************/

/**********************************************************
* Variable declarations
**********************************************************/

static neighbordiscovery_app_t app;

/**********************************************************
* Function Prototypes
**********************************************************/

/**********************************************************
* Functions
**********************************************************/

amxd_dm_t* neighbordiscovery_get_dm(void) {
    return app.dm;
}

amxo_parser_t* neighbordiscovery_get_parser(void) {
    return app.parser;
}

static int neighbordiscovery_init(amxd_dm_t* dm, amxo_parser_t* parser) {
    app.dm = dm;
    app.parser = parser;

    return 0;
}

static int neighbordiscovery_load_modules(void) {
    SAH_TRACEZ_IN(ME);
    int retval = 0;
    amxc_var_t lcontrollers;
    const amxc_var_t* controllers = NULL;
    amxm_shared_object_t* so = NULL;
    amxd_dm_t* dm = neighbordiscovery_get_dm();
    amxd_object_t* neighbordiscovery = amxd_dm_findf(dm, "NeighborDiscovery.");
    amxo_parser_t* parser = neighbordiscovery_get_parser();

    controllers = amxd_object_get_param_value(neighbordiscovery, "SupportedFWControllers");
    amxc_var_init(&lcontrollers);

    amxc_var_convert(&lcontrollers, controllers, AMXC_VAR_ID_LIST);
    amxc_var_for_each(controller, &lcontrollers) {
        amxc_string_t mod_path;

        amxc_string_init(&mod_path, 0);
        const char* name = GET_CHAR(controller, NULL);
        amxc_string_setf(&mod_path, "%s%s.so", "/usr/lib/amx/modules/", name);
        amxc_string_resolve_var(&mod_path, &parser->config);
        retval = amxm_so_open(&so, name, amxc_string_get(&mod_path, 0));
        if(retval != 0) {
            SAH_TRACEZ_WARNING(ME, "Failed to load %s", amxc_string_get(&mod_path, 0));
        }
        amxc_string_clean(&mod_path);
        when_failed(retval, exit);
    }
exit:
    amxc_var_clean(&lcontrollers);
    SAH_TRACEZ_OUT(ME);
    return retval;
}

int _neighbordiscovery_main(int reason,
                            amxd_dm_t* dm,
                            amxo_parser_t* parser) {
    SAH_TRACEZ_IN(ME);
    int retval = 1;
    switch(reason) {
    case 0:     // START
        when_false_trace(netmodel_initialize() == 1, exit, ERROR, "Failed to init netmodel");

        retval = neighbordiscovery_init(dm, parser);
        when_false_trace(!retval, exit, ERROR, "Failed to init neighbordiscovery_init");
        retval = neighbordiscovery_netmodel_intf_init();
        when_false_trace(!retval, exit, ERROR, "Failed to init neighbordiscovery_netmodel_intf_init");
        retval = neighbordiscovery_load_modules();
        when_false_trace(!retval, exit, ERROR, "Failed to init neighbordiscovery_netmodel_intf_init");
        break;
    case 1:     // STOP
        neighbordiscovery_netmodel_intf_cleanup();
        netmodel_cleanup();
        amxm_close_all();

        app.dm = NULL;
        app.parser = NULL;
        break;
    }
    retval = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

