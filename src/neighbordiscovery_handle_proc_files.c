/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**********************************************************
* Include files
**********************************************************/

#include "neighbordiscovery_handle_proc_files.h"
#include "neighbordiscovery_object_info.h"
#include "neighbordiscovery.h"
#include "neighbordiscovery_protocol.h"
#include "firewall_utility.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <linux/if_arp.h>

/**********************************************************
* Macro definitions
**********************************************************/

#define PATH_ROUTER_SOLICITATION_MAX_INTERVAL "/proc/sys/net/ipv6/conf/%s/router_solicitation_max_interval"
#define PATH_ACCEPT_RA "/proc/sys/net/ipv6/conf/%s/accept_ra"
#define PATH_DISABLE_IPV6 "/proc/sys/net/ipv6/conf/%s/disable_ipv6"

#define INTERFACE_SETTING_RSENABLE "RSEnable"
#define INTERFACE_SETTING_AUTOCONF "AutoConfEnable"

/**********************************************************
* Type definitions
**********************************************************/

typedef struct {
    const char* name;
    const char* file_path;
    uint32_t (* handle_param)(const uint32_t value, const cstring_t intf_name);
    uint32_t default_value;
} interfacesetting_param_t;

/**********************************************************
* Variable declarations
**********************************************************/

/**********************************************************
* Function Prototypes
**********************************************************/
static void file_write(const cstring_t intf_name, const cstring_t file_path, uint32_t value);
static int file_read(const cstring_t path, char* buf, uint32_t size);
static uint32_t file_read_uint32(const cstring_t path);
static uint32_t handle_RtrSolicitationInterval(const uint32_t value, const cstring_t intf_name);
static uint32_t handle_RSEnable(const uint32_t value, const cstring_t intf_name);

static interfacesetting_param_t interfacesetting_params[] = {
    { "DADTransmits", "/proc/sys/net/ipv6/conf/%s/dad_transmits", NULL, 1 },
    { "RetransTimer", "/proc/sys/net/ipv6/neigh/%s/retrans_time_ms", NULL, 1000 },
    { "RtrSolicitationInterval", "/proc/sys/net/ipv6/conf/%s/router_solicitation_interval", handle_RtrSolicitationInterval, 4000 },
    { "MaxRtrSolicitations", "/proc/sys/net/ipv6/conf/%s/router_solicitations", NULL, 3 },
    { "NUDEnable", NULL, NULL, 1 },
    { INTERFACE_SETTING_RSENABLE, "/proc/sys/net/ipv6/conf/%s/forwarding", handle_RSEnable, 0 },
    { INTERFACE_SETTING_AUTOCONF, "/proc/sys/net/ipv6/conf/%s/autoconf", NULL, 1 },
    { "AcceptDefaultRoute", "/proc/sys/net/ipv6/conf/%s/accept_ra_defrtr", NULL, 1 },
    { NULL, NULL, NULL, 0 }
};

/**********************************************************
* Functions
**********************************************************/

static uint32_t handle_RtrSolicitationInterval(const uint32_t value, const cstring_t intf_name) {
    SAH_TRACEZ_IN(ME);
    uint32_t ret_value = value;

    if(value != 0) {
        ret_value = value / 1000;
    }
    file_write(intf_name, PATH_ROUTER_SOLICITATION_MAX_INTERVAL, ret_value);
    SAH_TRACEZ_OUT(ME);
    return ret_value;
}

static uint32_t handle_RSEnable(const uint32_t value, const cstring_t intf_name) {
    SAH_TRACEZ_IN(ME);

    file_write(intf_name, PATH_ACCEPT_RA, value);

    SAH_TRACEZ_OUT(ME);
    return (value == 0 ? 1 : 0);
}

static int file_read(const cstring_t path, char* buf, uint32_t size) {
    SAH_TRACEZ_IN(ME);
    FILE* fp = NULL;
    int rv = -1;
    char* s = NULL;

    when_null(buf, exit);

    fp = fopen(path, "r");
    when_null_trace(fp, exit, ERROR, "Failed to open %s", path);
    s = fgets(buf, size, fp);
    when_null_trace(s, exit, ERROR, "Failed to read %s", path);

    rv = 0;
exit:
    if(fp != NULL) {
        fclose(fp);
        fp = NULL;
    }
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static uint32_t file_read_uint32(const cstring_t path) {
    SAH_TRACEZ_IN(ME);
    uint32_t result = 0;
    char buf[32] = {0};

    when_failed_trace(file_read(path, buf, sizeof(buf)), exit, ERROR, "Failed to read file %s", path);

    result = strtoull(buf, NULL, 10);
exit:
    SAH_TRACEZ_OUT(ME);
    return result;
}

static void file_write(const cstring_t intf_name, const cstring_t file_path, uint32_t value) {
    SAH_TRACEZ_IN(ME);
    char path[100] = {0};
    FILE* fp = NULL;
    uint32_t read_value = 0;
    int ret_val = -1;

    when_null_trace(intf_name, exit, ERROR, "Bad intf_name param given");
    when_null_trace(file_path, exit, ERROR, "Bad parameter id given, has an empty file_path");

    snprintf(path, 100, file_path, intf_name);

    read_value = file_read_uint32(path);

    if(read_value != value) {
        fp = fopen(path, "w");
        when_null_trace(fp, exit, ERROR, "Failed to open file at path %s", path);

        ret_val = fprintf(fp, "%u", value);
        when_false_trace(ret_val >= 0, exit, ERROR, "Failed to write to the file at path %s", path);
        SAH_TRACEZ_INFO(ME, "Successfully written the value %u to %s", value, path);
    } else {
        SAH_TRACEZ_INFO(ME, "No need to write %u to %s", value, path);
    }
exit:
    if(fp != NULL) {
        fclose(fp);
        fp = NULL;
    }
    SAH_TRACEZ_OUT(ME);
    return;
}

static void file_write_param(const cstring_t intf_name, const int parameter_id, uint32_t value) {
    SAH_TRACEZ_IN(ME);
    uint32_t handled_value = value;

    if(interfacesetting_params[parameter_id].handle_param != NULL) {
        handled_value = interfacesetting_params[parameter_id].handle_param(value, intf_name);
    }
    file_write(intf_name, interfacesetting_params[parameter_id].file_path, handled_value);
    SAH_TRACEZ_OUT(ME);
}

static amxd_status_t file_set_parameter(const cstring_t intf_name, const cstring_t changed_param_name, uint32_t value) {
    SAH_TRACEZ_IN(ME);
    bool found = false;
    amxd_status_t status = amxd_status_unknown_error;

    when_null_trace(intf_name, exit, ERROR, "Bad intf_name param given");
    when_null_trace(changed_param_name, exit, ERROR, "Bad changed_param_name param given");

    for(int i = 0; interfacesetting_params[i].name; i++) {
        if(strcmp(changed_param_name, interfacesetting_params[i].name) == 0) {
            found = true;
            file_write_param(intf_name, i, value);
            break;
        }
    }
    if(!found) {
        SAH_TRACEZ_WARNING(ME, "Did not find the changed param '%s'", changed_param_name);
    }
    status = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

void neighbordiscovery_file_update_intf(amxd_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    uint32_t value = 0;
    neigh_info_t* info = NULL;

    when_null_trace(obj, exit, ERROR, "no object found");
    info = (neigh_info_t*) obj->priv;
    when_null_trace(info, exit, ERROR, "no neigh_info_t found");
    when_str_empty_trace(info->intf_name, exit, ERROR, "no intf name found");

    firewall_open_neigh_port(obj);

    for(int i = 0; interfacesetting_params[i].name; i++) {
        value = amxd_object_get_value(uint32_t, obj, interfacesetting_params[i].name, NULL);
        file_write_param(info->intf_name, i, value);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void neighbordiscovery_file_reset_intf(amxd_object_t* obj, bool fw_close_port) {
    SAH_TRACEZ_IN(ME);
    neigh_info_t* info = NULL;

    when_null_trace(obj, exit, ERROR, "no object found");
    info = (neigh_info_t*) obj->priv;
    when_null_trace(info, exit, ERROR, "no neigh_info_t found");
    when_null(info->intf_name, exit);

    if(fw_close_port) {
        firewall_close_neigh_port(obj);
    }
    for(int i = 0; interfacesetting_params[i].name; i++) {
        file_write_param(info->intf_name, i, interfacesetting_params[i].default_value);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _neighbordiscovery_interfacesetting_param_changed(UNUSED const char* const event_name,
                                                       const amxc_var_t* const event_data,
                                                       UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);

    neigh_info_t* info = NULL;
    const cstring_t changed_param_name = NULL;
    amxc_var_t* changed_parameters = NULL;
    amxd_object_t* obj = amxd_dm_signal_get_object(neighbordiscovery_get_dm(), event_data);

    when_null_trace(obj, exit, ERROR, "Bad obj param var given");
    info = (neigh_info_t*) obj->priv;
    when_null_trace(info, exit, ERROR, "no neigh_info_t found");
    when_str_empty(info->intf_name, exit);
    when_null_trace(event_data, exit, ERROR, "Bad event_data param var given");
    changed_parameters = GET_ARG(event_data, "parameters");
    when_null_trace(changed_parameters, exit, ERROR, "Bad changed param var given");

    amxc_var_for_each(changed_param, changed_parameters) {
        changed_param_name = amxc_var_key(changed_param);
        when_null_trace(changed_param_name, exit, ERROR, "Failed to get the changed param name, for intf %s", info->intf_name);

        file_set_parameter(info->intf_name, changed_param_name, GET_UINT32(changed_param, "to"));

        if(strcmp(changed_param_name, INTERFACE_SETTING_RSENABLE) == 0) {
            //RSEnable defines which firewall port should be open -> so refresh it when the RSEnable param changes
            firewall_close_neigh_port(obj);
            firewall_open_neigh_port(obj);
        }
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}
