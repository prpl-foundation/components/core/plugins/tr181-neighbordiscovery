/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**********************************************************
* Include files
**********************************************************/

#include "neighbordiscovery_netmodel_intf.h"
#include "neighbordiscovery_object_info.h"
#include "neighbordiscovery_handle_proc_files.h"
#include "neighbordiscovery.h"
#include "neighbordiscovery_protocol.h"
#include "netmodel/client.h"

#include <amxd/amxd_action.h>
#include <amxd/amxd_transaction.h>

#include <stdlib.h>
#include <string.h>

/**********************************************************
* Macro definitions
**********************************************************/
#define STR_EMPTY(x) (((x) == NULL) || ((x)[0] == '\0'))
/**********************************************************
* Type definitions
**********************************************************/

typedef enum interfacesetting_status {
    status_enabled,
    status_disabled,
    status_error_misconfigured,
    status_error
} interfacesetting_status_t;

/**********************************************************
* Variable declarations
**********************************************************/

/**********************************************************
* Function Prototypes
**********************************************************/

static void add_netmodel_query(amxd_object_t* obj);
static void remove_netmodel_query(amxd_object_t* obj);
static void neighbordiscovery_set_status(amxd_object_t* obj, interfacesetting_status_t status);
static void neighbordiscovery_netmodel_intf_changed_cb(const char* const sig_name, const amxc_var_t* const data, void* const priv);
static void neighbordiscovery_netmodel_netdev_up_cb(const char* const sig_name, const amxc_var_t* const data, void* const priv);

/**********************************************************
* Functions
**********************************************************/

static void add_netmodel_query(amxd_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    bool instance_enabled = false;
    bool parent_enabled = false;
    neigh_info_t* info = NULL;

    when_null_trace(obj, exit, ERROR, "no object found");
    info = (neigh_info_t*) obj->priv;
    when_null_trace(info, exit, ERROR, "no neigh_info_t found");
    when_null_trace(info->obj, exit, ERROR, "no obj found in the neigh_info_t");
    when_false_trace(info->intf_query == NULL, exit, ERROR, "intf query should be empty");
    when_false_trace(info->intf_path == NULL, exit, ERROR, "intf_path should be empty");
    when_false_trace(info->intf_name == NULL, exit, ERROR, "intf_name should be empty");

    instance_enabled = amxd_object_get_value(bool, obj, "Enable", NULL);
    parent_enabled = amxd_object_get_value(bool, amxd_object_findf(obj, "^.^"), "Enable", NULL);
    if((parent_enabled == false) || (instance_enabled == false)) {
        goto exit;
    }

    info->intf_path = amxd_object_get_value(cstring_t, info->obj, "Interface", NULL);
    when_null_trace(info->intf_path, exit, ERROR, "the Interface parameter could not be found");

    info->intf_query = netmodel_openQuery_getFirstParameter(info->intf_path, "neighbordiscovery", "NetDevName", "netdev-bound && ipv6", netmodel_traverse_down, neighbordiscovery_netmodel_intf_changed_cb, (void*) info);
    if(info->intf_query == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to add a query to netmodel, make sure the interface param has a valid value: %s", info->intf_path);
        neighbordiscovery_set_status(info->obj, status_error_misconfigured);
        free(info->intf_path);
        info->intf_path = NULL;
        goto exit;
    } else {
        SAH_TRACEZ_INFO(ME, "Successfully added a query to netmodel, for the default neighbordiscovery intf %s", info->intf_path);
    }

    info->netdev_up_query = netmodel_openQuery_getFirstParameter(info->intf_path, "neighbordiscovery", "NetDevName", "netdev-up && ipv6", netmodel_traverse_down, neighbordiscovery_netmodel_netdev_up_cb, (void*) info);
    if(info->netdev_up_query == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to add address query to netmodel, make sure the interface param has a valid value: %s", info->intf_path);
    } else {
        SAH_TRACEZ_INFO(ME, "Successfully added address query to netmodel, for the default neighbordiscovery intf %s", info->intf_path);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void neighbordiscovery_netmodel_intf_changed_cb(UNUSED const char* const sig_name, const amxc_var_t* const data, void* const priv) {
    SAH_TRACEZ_IN(ME);
    neigh_info_t* info = (neigh_info_t*) priv;
    const char* intf_name = NULL;
    interfacesetting_status_t status = status_error_misconfigured;
    bool valid_value = false;
    bool valid_value_prev = false;

    when_null_trace(info, exit, ERROR, "no neigh_info_t found in the object");
    when_null_trace(info->obj, exit, ERROR, "Not able to find the default route info object");
    when_null_trace(data, exit, ERROR, "Invalid data parameter");

    intf_name = GET_CHAR(data, NULL);
    valid_value = (intf_name != NULL) && (intf_name[0] != '\0');
    valid_value_prev = (info->intf_name != NULL) && (info->intf_name[0] != '\0');

    if(!valid_value && !valid_value_prev) {
        goto exit_ignore;
    }
    if(valid_value && valid_value_prev && (strcmp(info->intf_name, intf_name) == 0)) {
        goto exit_ignore;
    }

    if(valid_value_prev) {
        neighbordiscovery_file_reset_intf(info->obj, false);
        free(info->intf_name);
        info->intf_name = NULL;
    }

    if(valid_value) {
        info->intf_name = strdup(intf_name);
        neighbordiscovery_file_update_intf(info->obj);
        status = status_enabled;
    } else {
        status = status_disabled;
    }
exit:
    neighbordiscovery_set_status(info->obj, status);
exit_ignore:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void neighbordiscovery_netmodel_mac_cb(UNUSED const char* const sig_name, const amxc_var_t* const data, void* const priv) {
    rs_info_t* rs_info = (rs_info_t*) priv;
    when_null(rs_info, exit);

    free(rs_info->mac);
    rs_info->mac = amxc_var_dyncast(cstring_t, data);

exit:
    return;
}

static void neighbordiscovery_netmodel_netdev_up_cb(UNUSED const char* const sig_name, const amxc_var_t* const data, void* const priv) {
    SAH_TRACEZ_IN(ME);
    neigh_info_t* info = (neigh_info_t*) priv;
    rs_info_t* rs_info = NULL;
    amxd_object_t* object = NULL;
    amxc_var_t params;
    bool rs_enable = false;

    amxc_var_init(&params);
    when_null(priv, exit);

    object = info->obj;
    when_null(object, exit);

    amxd_object_get_params(object, &params, amxd_dm_access_protected);
    rs_enable = GET_BOOL(&params, "RSEnable");

    if(!rs_enable) {
        SAH_TRACEZ_INFO(ME, "No need to send RS, RSEnable is false for intf %s", GET_CHAR(data, NULL));
        goto exit;
    }

    rs_info = &info->rs_info;

    if(rs_info->mac_query == NULL) {
        rs_info->mac_query = netmodel_openQuery_getFirstParameter(info->intf_path, "neighbordiscovery", "MACAddress", "", netmodel_traverse_down, neighbordiscovery_netmodel_mac_cb, (void*) rs_info);
    }
    if(STR_EMPTY(GET_CHAR(data, NULL))) {
        amxp_timer_stop(rs_info->rs_timer);
        rs_info->nr_rs = 0;
    } else {
        rs_info->rs_interval = GET_UINT32(&params, "RtrSolicitationInterval");
        rs_info->nr_rs = GET_UINT32(&params, "MaxRtrSolicitations");
        when_failed_trace(amxp_timer_start(rs_info->rs_timer, rs_info->rs_interval), exit, ERROR, "Failed to start RS timer");
    }

exit:
    amxc_var_clean(&params);
    SAH_TRACEZ_OUT(ME);
    return;
}

static void remove_netmodel_query(amxd_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    neigh_info_t* info = NULL;

    when_null_trace(obj, exit, ERROR, "no object found");
    info = (neigh_info_t*) obj->priv;
    when_null_trace(info, exit, ERROR, "no neigh_info_t found");
    when_null(info->intf_query, exit);

    neighbordiscovery_file_reset_intf(info->obj, true);
    neighbordiscovery_info_clear_content(info);
    SAH_TRACEZ_INFO(ME, "Successfully removed a query to netmodel");
    neighbordiscovery_set_status(obj, status_disabled);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void neighbordiscovery_set_status(amxd_object_t* obj, interfacesetting_status_t status) {
    SAH_TRACEZ_IN(ME);
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    switch(status) {
    case status_enabled:
        amxd_trans_set_value(cstring_t, &trans, "Status", "Enabled");
        break;
    case status_disabled:
        amxd_trans_set_value(cstring_t, &trans, "Status", "Disabled");
        break;
    case status_error_misconfigured:
        amxd_trans_set_value(cstring_t, &trans, "Status", "Error_Misconfigured");
        break;
    case status_error:
        amxd_trans_set_value(cstring_t, &trans, "Status", "Error");
        break;
    default:
        amxd_trans_set_value(cstring_t, &trans, "Status", "Error_Misconfigured");
        SAH_TRACEZ_ERROR(ME, "Invalid status '%d' value is given", status);
    }
    if(amxd_trans_apply(&trans, neighbordiscovery_get_dm()) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to apply the transaction for setting the status");
    }

    SAH_TRACEZ_OUT(ME);
    amxd_trans_clean(&trans);
}

int neighbordiscovery_netmodel_intf_init(void) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* neighbordiscovery_obj = amxd_dm_findf(neighbordiscovery_get_dm(), "NeighborDiscovery.InterfaceSetting.");
    amxd_object_iterate(instance, it, neighbordiscovery_obj) {
        amxd_object_t* inst = amxc_container_of(it, amxd_object_t, it);
        if(inst->priv == NULL) {
            neighbordiscovery_info_create(inst);
            add_netmodel_query(inst);
        }
    }
    SAH_TRACEZ_OUT(ME);
    return 0;
}

void neighbordiscovery_netmodel_intf_cleanup(void) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* neighbordiscovery_obj = amxd_dm_findf(neighbordiscovery_get_dm(), "NeighborDiscovery.InterfaceSetting.");
    amxd_object_iterate(instance, it, neighbordiscovery_obj) {
        amxd_object_t* inst = amxc_container_of(it, amxd_object_t, it);
        neighbordiscovery_file_reset_intf(inst, true);
        neighbordiscovery_info_clear(inst);
    }
    SAH_TRACEZ_OUT(ME);
}

void _neighbordiscovery_enable_changed(UNUSED const char* const event_name,
                                       const amxc_var_t* const event_data,
                                       UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* neighbordiscovery_inst = NULL;
    neigh_info_t* info = NULL;
    amxd_object_t* instance = amxd_dm_signal_get_object(neighbordiscovery_get_dm(), event_data);
    bool global_enabled = false;
    bool instance_enable = false;

    when_null_trace(instance, exit, ERROR, "Failed to find the changed instance");

    global_enabled = amxd_object_get_value(bool, instance, "Enable", NULL);
    amxd_object_iterate(instance, it, amxd_object_get(instance, "InterfaceSetting")) {
        neighbordiscovery_inst = amxc_container_of(it, amxd_object_t, it);
        instance_enable = amxd_object_get_value(bool, neighbordiscovery_inst, "Enable", NULL);
        info = (neigh_info_t*) neighbordiscovery_inst->priv;
        if(instance_enable && (info != NULL)) {
            if(global_enabled == true) {
                if(info->intf_query == NULL) {
                    add_netmodel_query(neighbordiscovery_inst);
                }
            } else {
                remove_netmodel_query(neighbordiscovery_inst);
            }
        }
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _neighbordiscovery_interfacesetting_enable_changed(UNUSED const char* const event_name,
                                                        const amxc_var_t* const event_data,
                                                        UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* instance = amxd_dm_signal_get_object(neighbordiscovery_get_dm(), event_data);
    bool is_enabled = false;

    when_null_trace(instance, exit, ERROR, "Failed to find the changed instance");

    is_enabled = amxd_object_get_value(bool, instance, "Enable", NULL);
    if(is_enabled) {
        add_netmodel_query(instance);
    } else {
        remove_netmodel_query(instance);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _neighbordiscovery_interfacesetting_interface_changed(UNUSED const char* const event_name,
                                                           UNUSED const amxc_var_t* const event_data,
                                                           UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* instance = amxd_dm_signal_get_object(neighbordiscovery_get_dm(), event_data);

    when_null_trace(instance, exit, ERROR, "Failed to find the changed instance");

    remove_netmodel_query(instance);
    add_netmodel_query(instance);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _neighbordiscovery_interfacesetting_added(UNUSED const char* const event_name,
                                               const amxc_var_t* const event_data,
                                               UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* instance = NULL;
    amxd_object_t* obj = amxd_dm_signal_get_object(neighbordiscovery_get_dm(), event_data);

    when_null_trace(obj, exit, ERROR, "Failed to find the added instance");
    instance = amxd_object_get_instance(obj, NULL, GET_UINT32(event_data, "index"));
    when_null_trace(instance, exit, ERROR, "Failed to find the added instance");

    if(instance->priv == NULL) {
        neighbordiscovery_info_create(instance);
        add_netmodel_query(instance);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

amxd_status_t _neighbordiscovery_interfacesetting_deleted(amxd_object_t* object,
                                                          amxd_param_t* param,
                                                          amxd_action_t reason,
                                                          const amxc_var_t* const args,
                                                          amxc_var_t* const retval,
                                                          void* priv) {
    SAH_TRACEZ_IN(ME);
    when_null(object, exit);

    neighbordiscovery_file_reset_intf(object, true);
    neighbordiscovery_info_clear(object);
exit:
    SAH_TRACEZ_OUT(ME);
    return amxd_action_object_destroy(object, param, reason, args, retval, priv);
}
