/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**********************************************************
* Include files
**********************************************************/
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>
#include <netinet/ip6.h>
#include <netinet/icmp6.h>
#include <netinet/ether.h>
#include <net/ethernet.h>
#include <sys/socket.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "netmodel/client.h"

#include "neighbordiscovery_protocol.h"
#include "neighbordiscovery_object_info.h"

/**********************************************************
* Macro definitions
**********************************************************/

/**********************************************************
* Type definitions
**********************************************************/

/**********************************************************
* Variable declarations
**********************************************************/

/**********************************************************
* Function Prototypes
**********************************************************/

/**********************************************************
* Functions
**********************************************************/

#define ME "neighbordiscovery"

typedef struct rs_packet {
    struct nd_router_solicit hdr;
    struct nd_opt_hdr opt_hdr;
    char mac_addr[6];
} rs_packet_t;

static int rs_set_socket_opts(int fd, const char* ifname) {
    int rv = -1;
    when_str_empty_trace(ifname, exit, ERROR, "Empty interface name");

    // Let the kernel compute our checksums
    if(setsockopt(fd, IPPROTO_RAW, IPV6_CHECKSUM, &(int) {2}, sizeof(int)) < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to set IPv6_CHECKSUM sockopt");
        goto exit;
    }

    // Bind to one device
    if(setsockopt(fd, SOL_SOCKET, SO_BINDTODEVICE, ifname, strlen(ifname)) < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to bind socket to device %s", ifname);
        goto exit;
    }

    if(setsockopt(fd, SOL_SOCKET, SO_DONTROUTE, &(int) {1}, sizeof(int)) < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to set SO_DONTROUTE sockopt");
        goto exit;
    }

    if(setsockopt(fd, IPPROTO_IPV6, IPV6_MULTICAST_HOPS, &(int) {255}, sizeof(int)) < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to set IPv6_MULTICAST_HOPS sockopt");
        goto exit;
    }
    rv = 0;

exit:
    return rv;
}

void send_rs(const char* ifname, const char* mac_addr) {
    rs_packet_t packet = {0};
    struct sockaddr_in6 dest = {0};
    int sockfd = -1;
    struct nd_router_solicit* hdr = &packet.hdr;
    size_t len = sizeof(*hdr);

    hdr->nd_rs_type = ND_ROUTER_SOLICIT;
    when_str_empty_trace(ifname, exit, ERROR, "Intf name is empty, can't send RS");

    if(mac_addr != NULL) {
        struct ether_addr mac;

        if(ether_aton_r(mac_addr, &mac) != NULL) {
            struct nd_opt_hdr* opt_hdr = &packet.opt_hdr;

            opt_hdr->nd_opt_type = ND_OPT_SOURCE_LINKADDR;
            opt_hdr->nd_opt_len = 1; // nr of bytes
            memcpy(packet.mac_addr, mac.ether_addr_octet, 6);

            len = sizeof(packet);
        }
    }

    sockfd = socket(AF_INET6, SOCK_RAW, IPPROTO_ICMPV6);
    if(sockfd < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to open socket to send Router Solicitation on interface %s", ifname);
        goto exit;
    }

    when_failed_trace(rs_set_socket_opts(sockfd, ifname), exit, ERROR, "Failed to set socket options for socket on intf %s", ifname);

    dest.sin6_family = AF_INET6;
    inet_pton(AF_INET6, "ff02::2", &dest.sin6_addr);

    if(sendto(sockfd, &packet, len, MSG_DONTWAIT, (struct sockaddr*) &dest, sizeof(dest)) < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to send RS on interface %s", ifname);
    }

exit:
    close(sockfd);
    return;
}

void rs_timer_cb(amxp_timer_t* timer, void* priv) {
    neigh_info_t* info = priv;
    rs_info_t* rs_info = NULL;
    when_null_trace(info, exit, ERROR, "Timer private data is NULL");
    when_str_empty_trace(info->intf_name, exit, ERROR, "Interface name is empty");

    rs_info = &info->rs_info;

    if(!netmodel_hasFlag(info->intf_path, "iprouter-up", "", netmodel_traverse_down)) {
        if(rs_info->nr_rs > 0) {
            SAH_TRACEZ_WARNING(ME, "Sending RS on interface %s with mac %s", info->intf_name, rs_info->mac);

            send_rs(info->intf_name, rs_info->mac);
            rs_info->nr_rs--;
            amxp_timer_start(timer, rs_info->rs_interval);
        }
    } else {
        SAH_TRACEZ_INFO(ME, "Already received RA so no need to send RS");

    }
exit:
    return;
}
