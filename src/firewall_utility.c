/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdint.h>
#include <stdlib.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxb/amxb.h>

#include "neighbordiscovery.h"
#include "firewall_utility.h"
#include "neighbordiscovery_object_info.h"

#define ME_fw "fwl"

/**********************************************************
* Type definitions
**********************************************************/

/**********************************************************
* Functions
**********************************************************/

static const char* get_fw_module_name(void) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* neigh_obj = NULL;
    const char* fw_module_name = NULL;

    neigh_obj = amxd_dm_findf(neighbordiscovery_get_dm(), "NeighborDiscovery.InterfaceSetting.");
    when_null_trace(neigh_obj, exit, ERROR, "Failed to get the NeighborDiscovery object");

    fw_module_name = amxc_var_constcast(cstring_t, amxd_object_get_param_value(neigh_obj, "FWController"));
    when_str_empty_trace(fw_module_name, exit, ERROR, "Could not find the fw module name");
exit:
    SAH_TRACEZ_OUT(ME);
    return fw_module_name;
}

/**
   @brief
   Adds a firewall rule to open the ICMPV6 messages type on an interface to allow neighbor discovery.
   Calls fw.set_service function.

   @param obj The neighbor discovery object that need to have the icmpv6 messages on its interface open
 */
void firewall_open_neigh_port(amxd_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    neigh_info_t* info = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    int status = -1;
    const char* controller = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_null_trace(obj, exit, ERROR, "No object given for the firewall controller");
    info = (neigh_info_t*) obj->priv;
    when_null_trace(info, exit, ERROR, "Unable to get object private data");
    when_null_trace(info->intf_path, exit, ERROR, "Unable to get object interface");
    when_null_trace(info->intf_name, exit, ERROR, "Unable to get object interface name");
    when_str_empty_trace(info->alias_fw_rule, exit, ERROR, "Firewall rule alias should not be empty");

    controller = get_fw_module_name();
    when_str_empty_trace(controller, exit, ERROR, "Could not find the fw module name");

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "id", info->alias_fw_rule);
    amxc_var_add_key(uint32_t, &args, "icmp_type", amxd_object_get_value(bool, obj, "RSEnable", NULL) ? ND_ICMPV6_RA : ND_ICMPV6_RS);
    amxc_var_add_key(bool, &args, "enable", true);
    amxc_var_add_key(cstring_t, &args, "interface", info->intf_path);
    amxc_var_add_key(uint32_t, &args, "ipversion", 6);
    amxc_var_add_key(cstring_t, &args, "protocol", "58");
    status = amxm_execute_function(controller,
                                   FIREWALL_MODULE,
                                   "set_service",
                                   &args,
                                   &ret);
    when_failed_trace(status, exit, ERROR, "Unable to add firewall rule returned %d.", status);
    SAH_TRACEZ_INFO(ME_fw, "Opening firewall icmpv6 messages for interface '%s'...", info->intf_name);
exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
}

/**
   @brief
   Deletes the firewall rule to allow icmpv6 messages on an interface.
   Calls Firewall.deleteService function.

   @param obj The neighbor discovery object that need to have the icmpv6 messages on its interface close
 */
void firewall_close_neigh_port(amxd_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    neigh_info_t* info = NULL;
    const char* controller = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    int status = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_null_trace(obj, exit, ERROR, "No object given for the firewall controller");
    when_null_trace(obj->priv, exit, ERROR, "Unable to get object private data.");
    info = (neigh_info_t*) obj->priv;
    when_null_trace(info->intf_name, exit, ERROR, "Unable to get object interface name");
    when_null_trace(info->alias_fw_rule, exit, ERROR, "Unable to get the alias_fw_rule");

    controller = get_fw_module_name();
    when_str_empty_trace(controller, exit, ERROR, "Could not find the fw module name");

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "id", info->alias_fw_rule);
    status = amxm_execute_function(controller,
                                   FIREWALL_MODULE,
                                   "delete_service",
                                   &args,
                                   &ret);

    when_failed_trace(status, exit, ERROR, "Unable to remove firewall rule returned %d.", status);
    SAH_TRACEZ_INFO(ME_fw, "Closing firewall service alias %s", info->intf_name);
exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
}